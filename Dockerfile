FROM node:14.15.4-alpine as builder
RUN mkdir -p /app
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY package.json  /app
RUN yarn install

ARG API
ENV REACT_APP_API ${API}

COPY . /app
RUN DISABLE_ESLINT_PLUGIN=true yarn build