export {Home} from './Home';
export {Services} from './Services';
export {Hire} from './Hire';
export {Contacts} from './Contacts';
export {InventoriesForSale} from './InventoriesForSale';
export {Inventory} from './Inventory';
export {NotFound} from './NotFound';
