export const startAction = type => ({type});
export const errorAction = ({type, error}) => ({type, error});
export const fetchAction = ({type, payload}) => ({type, payload});
