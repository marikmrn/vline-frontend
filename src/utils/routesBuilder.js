import {ROUTES} from '../contants';

const replaceNameAndId = originalRoute => (id, name) =>
  originalRoute
    .replace(':id', id)
    .replace(':name', name.replace(/ /g, '-').replace(/\//g, '-').replace(/\\/g, '-'));

export const getInventoryRoute = replaceNameAndId(ROUTES.INVENTORY);
