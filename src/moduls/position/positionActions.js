import {actionCreator} from '../../utils/actionCreator';
import {SET_DATA_POSITION, SET_MODAL_POSITION} from './positionTypes';

export const setDataPosition = data => actionCreator(SET_DATA_POSITION, data);

export const setModalPosition = payload => actionCreator(SET_MODAL_POSITION, payload);
